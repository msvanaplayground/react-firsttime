var React = require('react');
var ReactDOM = require('react-dom');

var data = [
  {id: 1, author: "Pete Hunt", text: "This is one comment"},
  {id: 2, author: "Jordan Walke", text: "This is *another* comment"}
];

CommentBox = React.createClass({
	getInitialState: function() {
    return {data: []};
  },

  handleCommentSubmit: function(comment) {
  	var allComments = this.state.data;
  	comment.id = allComments.length;
  	allComments.push(comment);
  	this.setState({data: allComments});
  },

	render: function() {
		return (
			<div className='commentBox'>
				<h1>Comments</h1>
				<CommentList data={this.state.data}/>
				<CommentForm onCommentSubmit={this.handleCommentSubmit}/>
			</div>
		);
	}
});

CommentForm = React.createClass({
	getInitialState: function() {
		return {author: '', text: ''};
	},

  handleAuthorChange: function(e) {
		this.setState({author: e.target.value});
	},

	handleTextChange: function(e) {
		this.setState({text: e.target.value});
	},

	handleFormSubmit: function(e) {
		e.preventDefault();
		var author = this.state.author.trim();
		var text = this.state.text.trim();

		if(!text || !author) return;

		this.setState({text: '', author: ''});
		this.props.onCommentSubmit({author: author, text: text});
	},

	render: function() {
		return (
			<form className='commentForm' onSubmit={this.handleFormSubmit}>
				<input type='text' 
				       placeholder='Your name' 
				       value={this.state.author} 
				       onChange={this.handleAuthorChange} />
				<input type='text' 
				       placeholder='Say something...'
				       value={this.state.text}
				       onChange={this.handleTextChange} />
				<input type='submit' value='Post' />
			</form>
		);
	}
});

CommentList = React.createClass({
	render: function() {
		var commentNodes = this.props.data.map(function(comment) {
			return (
				<Comment author={comment.author} key={comment.id}>
					{comment.text}
				</Comment>
			);
		});

		return (
			<div className='commentList'>
				{commentNodes}
			</div>
		);
	}
});

Comment = React.createClass({
	render: function() {
		return (
			<div className='comment'>
				<h2 className='commentAuthor'>
					{this.props.author}
				</h2>
				{this.props.children}
			</div>
		);
	}
});

ReactDOM.render(<CommentBox data={data} />, document.getElementById('application'));
